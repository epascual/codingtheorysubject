#include <iostream>
#include <algorithm>

using namespace std;

///Distancia entre dos vectores de longitud N
inline int dist(int a, int b, int N){
  int ret = 0;
  for (int i = 0, pos = 1; i < N; i++, pos*=2){
    /// Verifica si el bit n�mero i
    /// es igual o diferentes en ambos n�meros
    if ((a & pos)!=(b & pos)){
      ret++;
    }
  }
  return ret;
}

int main()
{
    #define N 5    ///Longitud de los vectores
    #define QN 32  ///Cantidad de vectores de longitud N
    #define K 5    ///Tama�o de los subconjuntos a analizar
    #define D 3    ///Distancia minima a encontrar

    ///Para representar los vectores
    ///utilizo los bits menos significativos
    ///de la representacion binaria de los numeros de 0 a 31

    ///Los 32 vectores de longitud 5
    int vectors[QN];
    for (int i=0; i < QN; i++)
      vectors[i] = i;

    ///Para obtener todos los subconjuntos de tama�o 5
    ///utilizo la funcion de biblioteca estandar de C++
    ///next_permutation.
    ///Esta me itera por todas las permutaciones de un arreglo
    ///con 5 unos y el resto de elementos 0. Cada permutaci�n
    ///representa un subconjunto distinto.
    int order[QN];
    for (int i=0; i < QN; i++){
      order[i] = 0;
      if (i < K) order[i] = 1;
    }
    reverse(order, order+QN);

    ///Para calcular el total de subconjuntos, as�
    ///como los que satisfacen la propiedad
    int total = 0, found = 0;
    int subconj[K];

    do {
      ///Obtengo el subconjuto a partir de la permutacion
      ///del arreglo order
      for (int i=0, j=0; i < QN; i++){
        if (order[i] == 1){
          subconj[j++] = vectors[i];
        }
      }
      int distMin = N; ///Mayor distancia posible

      ///Calculo distancia par a par de todos los elementos
      for (int i=0; i < K; i++){
        for (int j=i+1; j < K; j++){
          distMin = min(distMin, dist(subconj[i], subconj[j], N));
        }
      }

      if (distMin >= D){
        //printCode(subconj, K, N);
        found++;
      }

      total++;
    } while (next_permutation(order, order+QN));

    cout << "Se encontraron " << found
         << " codigos con distancia minima >= " << D << "\n";
    cout << "Fueron analizados " << total << " subconjuntos!\n";

    return 0;
}

/** SALIDA:
Se encontraron 0 codigos con distancia minima >= 3
Fueron analizados 201376 subconjuntos!
*/
