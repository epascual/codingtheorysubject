#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <iostream>
                        /* Coeficientes cuadraticos */
unsigned int Aq[320] = {
 0, 8,16,24, 1, 9,17,25, 2,10,18,26, 3,11,19,27, /* Alfa={1,2} */
 4,12,20,28, 5,13,21,29, 6,14,22,30, 7,15,23,31,
 0, 4,16,20, 1, 5,17,21, 2, 6,18,22, 3, 7,19,23, /* Alfa={1,3} */
 8,12,24,28, 9,13,25,29,10,14,26,30,11,15,27,31,
 0, 2,16,18, 1, 3,17,19, 4, 6,20,22, 5, 7,21,23, /* Alfa={1,3} */
 8,10,24,26, 9,11,25,27,12,14,28,30,13,15,29,31,
 0, 1,16,17, 2, 3,18,19, 4, 5,20,21, 6, 7,22,23, /* Alfa={1,5} */
 8, 9,24,25,10,11,26,27,12,13,28,29,14,15,30,31,
 0, 4, 8,12, 1, 5, 9,13, 2, 6,10,14, 3, 7,11,15, /* Alfa={2,3} */
16,20,24,28,17,21,25,29,18,22,26,30,19,23,27,31,
 0, 2, 8,10, 1, 3, 9,11, 4, 6,12,14, 5, 7,13,15, /* Alfa={2,4} */
16,18,24,26,17,19,25,27,20,22,28,30,21,23,29,31,
 0, 1, 8, 9, 2, 3,10,11, 4, 5,12,13, 6, 7,14,15, /* Alfa={2,5} */
16,17,24,25,18,19,26,27,20,21,28,29,22,23,30,31,
 0, 2, 4, 6, 1, 3, 5, 7, 8,10,12,14, 9,11,13,15, /* Alfa={3,4} */
16,18,20,22,17,19,21,23,24,26,28,30,25,27,29,31,
 0, 1, 4, 5, 2, 3, 6, 7, 8, 9,12,13,10,11,14,15, /* Alfa={3,5} */
16,17,20,21,18,19,22,23,24,25,28,29,26,27,30,31,
 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15, /* Alfa={4,5} */
16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31};

                        /* Coeficientes lineales */
unsigned int Al[32*5] = {
 0,16, 1,17, 2,18, 3,19, 4,20, 5,21, 6,22, 7,23, /* Alfa={1} */
 8,24, 9,25,10,26,11,27,12,28,13,29,14,30,15,31,
 0, 8, 1, 9, 2,10, 3,11, 4,12, 5,13, 6,14, 7,15, /* Alfa={2} */
16,24,17,25,18,26,19,27,20,28,21,29,22,30,23,31,
 0, 4, 1, 5, 2, 6, 3, 7, 8,12, 9,13,10,14,11,15, /* Alfa={3} */
16,20,17,21,18,22,19,23,24,28,25,29,26,30,27,31,
 0, 2, 1, 3, 4, 6, 5, 7, 8,10, 9,11,12,14,13,15, /* Alfa={4} */
16,18,17,19,20,22,21,23,24,26,25,27,28,30,29,31,
 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15, /* Alfa={5} */
16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31};

/*  Palabras de codigo */
unsigned int PC1[16] = {  /* 16 comb. lin. para 1, v1, v2 y v3 */
0x00000000,0xffffffff,0xffff0000,0x0000ffff,0xff00ff00,0x00ff00ff,0x00ffff00,0xff0000ff,
0xf0f0f0f0,0x0f0f0f0f,0x0f0ff0f0,0xf0f00f0f,0x0ff00ff0,0xf00ff00f,0xf00f0ff0,0x0ff0f00f};

unsigned int PC2[16] = {  /* 16 comb. lin. para v4, v5, v12 y v13 */
0x00000000,0xcccccccc,0xaaaaaaaa,0x66666666,0xff000000,0x33cccccc,0x55aaaaaa,0x99666666,
0xf0f00000,0x3c3ccccc,0x5a5aaaaa,0x96966666,0x0ff00000,0xc33ccccc,0xa55aaaaa,0x69966666};

unsigned int PC3[16] = {  /* 16 comb. lin. para v14, v15, v23 y v24 */
0x00000000,0xcccc0000,0xaaaa0000,0x66660000,0xf000f000,0x3cccf000,0x5aaaf000,0x9666f000,
0xcc00cc00,0x00cccc00,0x66aacc00,0xaa66cc00,0x3c003c00,0xf0cc3c00,0x96aa3c00,0x5a663c00};

unsigned int PC4[16] = {  /* 16 comb. lin. para v25, v34, v35 y v45 */
0x00000000,0xaa00aa00,0xc0c0c0c0,0x6ac06ac0,0xa0a0a0a0,0x0aa00aa0,0x60606060,0xca60ca60,
0x88888888,0x22882288,0x48484848,0xe248e248,0x28282828,0x82288228,0xe8e8e8e8,0x42e842e8};

#define N 32
unsigned int DE1[N+1];  /* Distribucion de errores */
unsigned int DE2[N+1];
unsigned int DE3[N+1];

unsigned char xp[N];

int eval(unsigned int A[], unsigned int ns)
{
	unsigned int i, j, ne = N/(2*ns);
	unsigned char bit, vota[2] = {0,0};

	for(i=0;i<N;i+=ns) {
	    for(j=0,bit=0;j<ns;j++) bit ^= xp[A[i+j]];
	    vota[bit]++;
	}

	if(vota[0]>=ne)
    return(0);
	return(1);
}

int PH(int x)
{
	int i, p=0;

	for(i=0;i<N;i++,x >>= 1)
    p += (x&1);

	return(p);
}


double ipow(double x, int e){
  double ret = 1;
  for (int i=0; i < e; i++){
    ret *= x;
  }
  return ret;
}

int C[N+1][N+1];

double calculatePerrT(double p){
  double ret = 1;
  int T = 3; /// los errores que corrige: (8-1)/2

  for (int i=0; i <= N; i++) C[i][0] = C[i][i] = 1;
  for (int i=2; i <= N; i++){
    for (int j=1; j <= std::min(i,T+1); j++){
      C[i][j] = C[i-1][j-1] + C[i-1][j];
    }
  }

  for (int j=0; j <= T; j++){
    //std::cout << C[N][j] << "\n";
    ret -= C[N][j]*ipow(1.0-p,N-j)*ipow(p,j);
  }

  return ret;
}



int main(int argc, char *argv[])
{
	FILE *fp, *fin;
	unsigned int i, j, m, a, A, c1, c2, d1, d2, d3, d4, x, y, tot0=0, tot1=0, tot2=0,
	             tot3=0, x_, errorCanal=100; /// <----*

	if((fp = fopen(argv[1],"rb")) == NULL) {
	    printf("Error en el archivo original: %s\n",argv[1]);
	    return 0;
	}
	if((fin = fopen(argv[2],"rb")) == NULL) {
	    printf("Error en el archivo del canal: %s\n",argv[2]);
	    return 0;
	}
  errorCanal = atoi(argv[3]);                                        /// <----*

	memset(DE2,0,(N+1)*sizeof(unsigned int));
	while((c1=fgetc(fp))!=EOF) { ///Leyendo de fichero original
	    if((c2=fgetc(fp))==EOF)
        break;

	    y = PC1[c1&15] ^ PC2[(c1>>4)&15] ^ PC3[c2&15] ^ PC4[(c2>>4)&15];

	    ///Leyendo del canal
	    d1=fgetc(fin);  d2=fgetc(fin);   d3=fgetc(fin);   d4=fgetc(fin);
	    x = (d1&255) ^ ((d2&255)<<8) ^ ((d3&255)<<16) ^ ((d4&255)<<24);

	    DE1[PH(x^y)]++; ///error generado por el canal

	    memset(xp,0,N*sizeof(unsigned char));
	    for(i=0,m=1;i<N;i++,m<<=1) if(x&m) xp[i] = 1;
	    for(i=0,a=0;i<10;i++) a ^= eval(&Aq[i*N],4) << (6+i);
	    x ^= PC2[(a>>4)&15] ^ PC3[(a>>8)&15] ^ PC4[(a>>12)&15];
	    A = a;
	    memset(xp,0,N*sizeof(unsigned char));
	    for(i=0,m=1;i<N;i++,m<<=1) if(x&m) xp[i] = 1;
	    for(i=0,a=0;i<5;i++) a ^= eval(&Al[i*N],2) << (1+i);
	    x ^= PC1[a&15] ^ PC2[(a>>4)&15];
	    A ^= a;
	    if(PH(x)>(N/2)) A ^= 1;

	    ///A es la palabra de informacion estimada
/*          putchar(A&255);
            putchar((A>>8)&255);
	    if((A^(c1^(c2<<8)))) printf("Error\n");   */
	    DE2[PH(A^(c1^(c2<<8)))]++;

	    ///La codificación de A
	    //x_ = (d1&255) ^ ((d2&255)<<8) ^ ((d3&255)<<16) ^ ((d4&255)<<24);
	    x_ = PC1[A&15] ^ PC2[(A>>4)&15] ^ PC3[(A>>8)&15] ^ PC4[(A>>12)&15]; /// <----*

	    DE3[PH(y^x_)]++;                                                    /// <----*
	}
	fclose(fp);
	printf("\t\t\t\t      Wt(X-X')       Wt(X-X^)       Wt(A-A^)\n");
	for(i=0;i<N+1;i++) {
	    if((DE1[i]==0) && (DE2[i]==0) && (DE3[i]==0)) /// <----*
        continue;
	    tot0 += i*DE1[i];
	    tot1 += DE2[i];
	    tot2 += i*DE2[i];
	    if (i){
        tot3 += DE3[i];                             /// <----*
	    }
	    printf("\t  Errores de peso = %2d %14d %14d %14d\n",i,DE1[i],DE3[i],DE2[i]);
	}

	printf("P = %d/%d = %1.14f\n",tot0,tot1*32,((float) tot0)/((float) tot1*32));
	printf("Perr = %d/%d = %1.14f\n",tot3,tot1,((float) tot3)/((float) tot1));             /// <----*
	printf("Psymb = %d/%d = %1.14f\n",tot2,tot1*16,((float) tot2)/((float) tot1*16));
	printf("PerrT = %1.14f\n", calculatePerrT(1.0/(float)errorCanal));

	return 0;
}

