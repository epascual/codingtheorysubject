#include <stdio.h>

/*  Palabras de codigo */
unsigned int PC1[16] = {  /* 16 comb. lin. para 1, v1, v2 y v3 */
0x00000000,0xffffffff,0xffff0000,0x0000ffff,0xff00ff00,0x00ff00ff,0x00ffff00,0xff0000ff,
0xf0f0f0f0,0x0f0f0f0f,0x0f0ff0f0,0xf0f00f0f,0x0ff00ff0,0xf00ff00f,0xf00f0ff0,0x0ff0f00f};

unsigned int PC2[16] = {  /* 16 comb. lin. para v4, v5, v12 y v13 */
0x00000000,0xcccccccc,0xaaaaaaaa,0x66666666,0xff000000,0x33cccccc,0x55aaaaaa,0x99666666,
0xf0f00000,0x3c3ccccc,0x5a5aaaaa,0x96966666,0x0ff00000,0xc33ccccc,0xa55aaaaa,0x69966666};

unsigned int PC3[16] = {  /* 16 comb. lin. para v14, v15, v23 y v24 */
0x00000000,0xcccc0000,0xaaaa0000,0x66660000,0xf000f000,0x3cccf000,0x5aaaf000,0x9666f000,
0xcc00cc00,0x00cccc00,0x66aacc00,0xaa66cc00,0x3c003c00,0xf0cc3c00,0x96aa3c00,0x5a663c00};

unsigned int PC4[16] = {  /* 16 comb. lin. para v25, v34, v35 y v45 */
0x00000000,0xaa00aa00,0xc0c0c0c0,0x6ac06ac0,0xa0a0a0a0,0x0aa00aa0,0x60606060,0xca60ca60,
0x88888888,0x22882288,0x48484848,0xe248e248,0x28282828,0x82288228,0xe8e8e8e8,0x42e842e8};

int main(int argc, char *argv[])
{
	FILE *fp, *fout;
	int  c1, c2;
	unsigned int x;

	if((fp = fopen(argv[1],"rb")) == NULL) {
	    printf("Error en el archivo de lectura: %s\n",argv[1]);
	    return 0;
	}
	if((fout = fopen(argv[2],"wb")) == NULL) {
	    printf("Error en el archivo de escritura: %s\n",argv[2]);
	    return 0;
	}
	while((c1=fgetc(fp))!=EOF) {
	    if((c2=fgetc(fp))==EOF) break;
	    x = PC1[c1&15] ^ PC2[(c1>>4)&15] ^ PC3[c2&15] ^ PC4[(c2>>4)&15];
	    fputc(x&255, fout);
	    fputc((x>>8)&255, fout);
	    fputc((x>>16)&255, fout);
	    fputc((x>>24)&255, fout);
	}
	fclose(fp);
	fclose(fout);

	return 0;
}
