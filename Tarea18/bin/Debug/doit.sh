#/bin/bash

if [[ $# -lt 1 ]]
then
	echo "Usage: $0 [file_to_code]"
	exit
fi

error=100
if [[ $# -ge 2 ]]
then
	error=$2
fi


echo "Coding $1"

./codifica $1 $1_coded

echo "In a canal with error $error"

./canal $error $1_coded $1_noise

echo "Evaluating"
echo ""

./decodifica $1 $1_noise $error
