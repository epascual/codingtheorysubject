#include <stdio.h>
#include <stdlib.h>
#include <ctime>

#define N 8

int main(int argc, char *argv[])
{
	int  i, c, invp, e;

  FILE *fin, *fout;

  if((fin = fopen(argv[2],"rb")) == NULL) {
	    printf("Error en el archivo %s\n",argv[1]);
	    return 0;
	}
	if((fout = fopen(argv[3],"wb")) == NULL) {
	    printf("Error en el archivo %s\n",argv[1]);
	    return 0;
	}

	srand((int) time(0));
	invp = atoi(argv[1]);
	while((c=fgetc(fin))!=EOF) {
    e = 0;
    for(i=0;i<N;i++)
      if(!(rand()%invp))
        e ^= (1<<i);
    unsigned char t = c^e;
	  fputc(t, fout);
	}

	return 0;
}
