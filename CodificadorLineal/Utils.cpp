#include "Utils.h"

double ipow(double x, int e){
  double ret = 1;
  for (int i=0; i < e; i++){
    ret *= x;
  }
  return ret;
}
