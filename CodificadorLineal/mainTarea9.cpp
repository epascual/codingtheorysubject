#include <bits/stdc++.h>
#include "Matrix.h"
#include "LEncoderDecoder.h"
#include "LEncoderDecoderByStdArr.h"
#include "GF.h"
using namespace std;

/* Shortcut para referirse a GF2 */
typedef GF<2> GF2;

int main()
{
  cout << fixed << setprecision(8);

  cout << "Calculo de las tablas para la Tarea 9\n";

  Matrix<GF2>
  G_a (1, 5, {
    {1,1,1,1,1}
  }),
  H_b (1, 5, {
    {1,1,1,1,1}
  }),
  G_c (2, 5, {
    {1,0,1,1,0},
    {0,1,1,0,1}
  }),
  H_d (2, 5, {
    {1,0,1,1,0},
    {0,1,1,0,1}
  }),
  H_e (3, 7, {
    {0,1,1,1,1,0,0},
    {1,0,1,1,0,1,0},
    {1,1,0,1,0,0,1}
  }),
  G_f (4, 8, {
    {1,0,0,0,0,1,1,1},
    {0,1,0,0,1,0,1,1},
    {0,0,1,0,1,1,0,1},
    {0,0,0,1,1,1,1,0}
  }) ;

  double p = 0.01;
  cout << " Para inciso a\n";
  LEncoderDecoderByStdArr<GF2> inc_a =
        LEncoderDecoderByStdArr<GF2>::fromStandardG(G_a);
  inc_a.evaluate(p);

  cout << " Para inciso b\n";
  LEncoderDecoderByStdArr<GF2> inc_b =
        LEncoderDecoderByStdArr<GF2>::fromStandardH(H_b);
  inc_b.evaluate(p);

  cout << " Para inciso c\n";
  LEncoderDecoderByStdArr<GF2> inc_c =
        LEncoderDecoderByStdArr<GF2>::fromStandardG(G_c);
  inc_c.evaluate(p);

  cout << " Para inciso d\n";
  LEncoderDecoderByStdArr<GF2> inc_d =
        LEncoderDecoderByStdArr<GF2>::fromStandardH(H_d);
  inc_d.evaluate(p);

  cout << " Para inciso e\n";
  LEncoderDecoderByStdArr<GF2> inc_e =
        LEncoderDecoderByStdArr<GF2>::fromStandardH(H_e);
  inc_e.evaluate(p);

  cout << " Para inciso f\n";
  LEncoderDecoderByStdArr<GF2> inc_f =
        LEncoderDecoderByStdArr<GF2>::fromStandardG(G_f);
  inc_f.evaluate(p);

  inc_a.printStandardArray(); cout << "\n";
  inc_b.printStandardArray(); cout << "\n";
  inc_c.printStandardArray(); cout << "\n";
  inc_d.printStandardArray(); cout << "\n";
  inc_e.printStandardArray(); cout << "\n";
  inc_f.printStandardArray(); cout << "\n";

  return 0;
}
