#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

#include <vector>

using namespace std;

/** pow with integer exponent, I'm pretty sure that precission is better */
double ipow(double x, int e);

template<class Field>
int hammingWeight(const vector<Field> &v){
  int ret = 0;
  for (const Field &x : v){
    if (x != Field::ZERO())
      ret++;
  }
  return ret;
}

template<class Field>
int hammingWeight(const vector<Field> &v, int bound){
  int ret = 0;
  for (int i=0; i < bound; i++){
    const Field &x = v[i];
    if (x != Field::ZERO())
      ret++;
  }
  return ret;
}

template<class Field>
vector<Field> operator+ (const vector<Field> &a, const vector<Field> &b){
  assert(a.size() == b.size());
  vector<Field> ret(a.size());
  for (int i=0; i < (int)a.size(); i++){
    ret[i] = Field (a[i] + b[i]);
  }
  return ret;
}

template<class Field>
vector<Field> operator- (const vector<Field> &a, const vector<Field> &b){
  assert(a.size() == b.size());
  vector<Field> ret(a.size());
  for (int i=0; i < (int)a.size(); i++){
    ret[i] = Field (a[i] - b[i]);
  }
  return ret;
}

template<class Field>
bool operator< (const vector<Field> &a, const vector<Field> &b){
  assert(a.size() == b.size());
  for (int i=0; i < (int)a.size(); i++){
    if (a[i] != b[i]) return a[i] < b[i];
  }
  return false;
}




#endif // UTILS_H_INCLUDED
