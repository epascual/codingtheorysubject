#ifndef F16ENCODERDECODER_H
#define F16ENCODERDECODER_H

#include "LEncoderDecoder.h"
#include "F16.h"

class F16EncoderDecoder : public LEncoderDecoder<F16>
{
public:
  F16EncoderDecoder(const Matrix<F16> &_G, const Matrix<F16> &_H):
                    LEncoderDecoder<F16>(_G, _H)
  {}

  static F16EncoderDecoder fromStandardG(const Matrix<F16> &G){
    Matrix<F16> H = standardGtoH(G);
    return F16EncoderDecoder(G, H);
  }
   static F16EncoderDecoder fromStandardH(const Matrix<F16> &H){
    Matrix<F16> G = standardHtoG(H);
    return F16EncoderDecoder(G, H);
  }

  virtual vector<F16> decode(const vector<F16> &code){
    vector<F16> S = H*code;
    if (hammingWeight(S) == 0){
      return code;
      return LEncoderDecoder<F16>::decode(code);
    }

    if (S[0] != F16::ZERO()){
      vector<F16> C = S;
      C[0] = 1;
      C[1] = S[1]/S[0];
      for (int j=0; j < 16; j++){
        if (C[1] == H.data[1][j]){
          vector<F16> error = vector<F16> (17, F16::ZERO());
          error[j] = S[0];
          return code-error;
          return LEncoderDecoder<F16>::decode(code-error);
        }
      }
      /* ESTO NO DEBERIA PASAR*/
      assert(false && "no encontro la columna, esto es raro!");
      return LEncoderDecoder<F16>::decode(code);
    }

    vector<F16> error = vector<F16> (17, F16::ZERO());
    error[16] = S[1];
    return code-error;
    return LEncoderDecoder<F16>::decode(code-error);
  }
};

#endif // F16ENCODERDECODER_H
