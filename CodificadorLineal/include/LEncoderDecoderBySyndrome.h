#ifndef LEncoderDecoderBYSYNDROME_H_INCLUDED
#define LEncoderDecoderBYSYNDROME_H_INCLUDED

#include "LEncoderDecoder.h"

/**
  Para decodificar por syndrome*/
template<class Field>
class LEncoderDecoderBySyndrome : public LEncoderDecoder<Field> {

  using LEncoderDecoder<Field>::G;
  using LEncoderDecoder<Field>::H;
  using LEncoderDecoder<Field>::standardGtoH;
  using LEncoderDecoder<Field>::standardHtoG;

  map< vector<Field>, vector<Field> > syndromeToError;

  void generateCodes(int pos, int K, vector<vector<Field>> &codes, vector<Field> &info){
    if (pos == K){
      codes.push_back(this->encode(info));
      return;
    }
    for (Field felem : Field::ELEMS()){
      info[pos] = felem;
      generateCodes(pos+1, K, codes, info);
    }
  }

  static void generateQN(int pos, int N, set<vector<Field> > QN[], vector<Field> &elem){
    if (pos == N){
      int weight = hammingWeight(elem);
      QN[weight].insert(elem);
      return;
    }
    for (Field felem : Field::ELEMS()){
      elem[pos] = felem;
      generateQN(pos+1, N, QN, elem);
    }
  }

public:
  LEncoderDecoderBySyndrome(const Matrix<Field> &_G, const Matrix<Field> &_H):
                      LEncoderDecoder<Field>(_G, _H){
    int n = G.getWidth(), k = G.getHeight();

    vector<vector<Field>> codes;
    vector<Field> info(k);
    generateCodes(0, k, codes, info);

    cout << "Here1\n";

    /** La primera fila son los c�digos */
    vector<
      set<vector<Field> >
    > QN(n+1);
    vector<Field> elem(n);
    generateQN(0, n, &QN[0], elem);

    for (int errorSize=0; errorSize < n+1; errorSize++){
      while (!(QN[errorSize].empty())){
        auto error = *(QN[errorSize].begin());
        syndromeToError[H*error] = error;
        for (auto code : codes){
          auto elem = code + error;
          int weight = hammingWeight(elem);
          QN[weight].erase(QN[weight].find(elem));
        }
      }
    }

  }

  static LEncoderDecoderBySyndrome fromStandardG(const Matrix<Field> &G){
    Matrix<Field> H = standardGtoH(G);
    return LEncoderDecoderBySyndrome(G, H);
  }
   static LEncoderDecoderBySyndrome fromStandardH(const Matrix<Field> &H){
    Matrix<Field> G = standardHtoG(H);
    return LEncoderDecoderBySyndrome(G, H);
  }

  virtual vector<Field> decode(const vector<Field> &code){
    vector<Field> syndrome = H*code;
    vector<Field> error = syndromeToError[syndrome];
    return LEncoderDecoder<Field>::decode(code - error);
  }

  void printSyndromes(){
    for (auto syn : syndromeToError){
      cout << syn.second << " <-> " << syn.first << "\n";
    }
  }


/** Evaluation section */
private:
  double calculatePerr(double p){
    double ret = 1;
    int n = G.getWidth();
    vector<int> alpha(n+1, 0);
    for (auto syn: syndromeToError){
      alpha[hammingWeight(syn.second)]++;
    }
    for (int j=0; j <= n; j++){
      ret -= alpha[j]*ipow(1-p,n-j)*ipow(p,j);
    }
    return ret;
  }

public:
  virtual void evaluate(double p){
    double Perr;
    Perr = calculatePerr(p);
    cout << "Perr= " << Perr << "\n";
  }

};

#endif // LEncoderDecoderBYSYNDROME_H_INCLUDED
