#ifndef MATRIX_H
#define MATRIX_H

#include <bits/stdc++.h>
using namespace std;

template<class Field> class LEncoderDecoder;

/* T es el campo de los elementos de la matriz */
template<class T>
class Matrix {
  int height; ///Height o altura
  int width; ///Width o ancho
  vector< vector<T> > data;

public:

  /* Constructor para Matrix cero */
  Matrix(int h, int w){
    height = h; width = w;
    data = vector< vector<T> > (height, vector<T> (width, T::ZERO()));
  }

  /* Constructor a partir de arreglo bidimensional */
  Matrix(int h, int w, const vector< vector<T> > &ptr){
    height = h; width = w;
    data = vector< vector<T> > (height, vector<T> (width, T::ZERO()));
    for (int i=0; i < h; i++){
      for (int j=0; j < w; j++){
        data[i][j] = ptr[i][j];
      }
    }
  }

  /* Constructor de copia */
  Matrix(const Matrix &o){
    height = o.height;
    width = o.width;
    data = o.data;
  }

  Matrix& operator= (const Matrix &o) {
    height = o.height;
    width = o.width;
    data = o.data;
    return *this;
  }

  /* Getters */
  int getHeight() const { return height; }
  int getWidth() const { return width; }


  /* Construye y devuelve una Matrix identidad de orden n */
  static Matrix identidad(int n){
    Matrix ret(n,n);
    for (int i=0; i < n; i++)
      ret.data[i][i] = T::ONE();
    return ret;
  }

  /**** Operaciones entre matrices ****/
  /* Suma de matrices */
  Matrix operator+ (const Matrix &b) const{
    assert(height == b.height); /*Precondición, el mismo orden */
    assert(width == b.width);

    Matrix ret(height,width);
    for (int i=0; i < height; i++)
      for (int j=0; j < width; j++)
        ret.data[i][j] = data[i][j] + b.data[i][j];

    return ret;
  }

  /* Multiplicacion de matrices */
  Matrix operator* (const Matrix &b) const{
    assert(width == b.height);

    Matrix ret(height, b.width);
    for (int i=0; i < height; i++){
      for (int j=0; j < b.width; j++){
        for (int k=0; k < width; k++){
          ret.data[i][j] = ret.data[i][j] + data[i][k] * b.data[k][j];
        }
      }
    }

    return ret;
  }

  /* Multiplicacion de matriz por vector */
  vector<T> operator* (const vector<T> &b) const{
    assert(width == (int)b.size());

    Matrix bTrans = Matrix(1,b.size(), {b}).trans();
    Matrix mult = ((*this)*bTrans).trans();

    return mult.data[0];
  }
  /* Multiplicacion de vector por matriz */
  friend vector<T> operator* (const vector<T> &b, const Matrix &a) {
    assert(a.height == (int)b.size());

    Matrix bTrans = Matrix(1,b.size(), {b});
    Matrix mult = (bTrans*(a));

    return mult.data[0];
  }


  /* Matriz transpuesta */
  Matrix trans() const {
    Matrix ret(width, height);

    for (int i=0; i < height; i++){
      for (int j=0; j < width; j++){
        ret.data[j][i] = data[i][j];
      }
    }

    return ret;
  }

  /* Matriz por un escalar */
  friend Matrix operator* (const T &k, const Matrix &a){
    Matrix ret(a.height, a.width);
    for (int i=0; i < a.height; i++){
      for (int j=0; j < a.width; j++){
        ret.data[i][j] = k * a.data[i][j];
      }
    }
    return ret;
  }

  /* Para imprimir la matriz con cout */
  friend ostream& operator<< (ostream& os, const Matrix& a){
    for (int i=0; i < a.height; i++){
      for (int j=0; j < a.width; j++){
        if (j > 0) os << " ";
        os << a.data[i][j];
      }
      os << "\n";
    }
    return os;
  }

  template <class Field>
  friend  class LEncoderDecoder;

  friend class F16EncoderDecoder;
};



#endif // MATRIX_H
