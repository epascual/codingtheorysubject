#ifndef F16_H
#define F16_H

#include <string>
using namespace std;

class F16
{
protected:

  static string add[256];
  static string mult[256];

  static vector<F16> _ELEMS;
  static vector<F16> _INVERSOS;

  static F16 _ZERO;
  static F16 _ONE;

  char x;
  int toInt() const {
    if (isdigit(x)) return x - '0';
    else return x - 'a' + 10;
  }

public:

  static void init(){
    add['0'] = "0123456789abcdef";
    add['1'] = "1032547698badcfe";
    add['2'] = "23016745ab89efcd";
    add['3'] = "32107654ba98fedc";
    add['4'] = "45670123cdef89ab";
    add['5'] = "54761032dcfe98ba";
    add['6'] = "67452301efcdab89";
    add['7'] = "76543210fedcba98";
    add['8'] = "89abcdef01234567";
    add['9'] = "98badcfe10325476";
    add['a'] = "ab89efcd23016745";
    add['b'] = "ba98fedc32107654";
    add['c'] = "cdef89ab45670123";
    add['d'] = "dcfe98ba54761032";
    add['e'] = "efcdab8967452301";
    add['f'] = "fedcba9876543210";

    mult['0'] = "0000000000000000";
    mult['1'] = "0123456789abcdef";
    mult['2'] = "02468ace3175b9fd";
    mult['3'] = "0365cfa9b8de7412";
    mult['4'] = "048c37bf62ea51d9";
    mult['5'] = "05af72d8eb419c36";
    mult['6'] = "06cabd71539fe824";
    mult['7'] = "07e9f816da3425cb";
    mult['8'] = "083b6e5dc4f7a291";
    mult['9'] = "09182b3a4d5c6f7e";
    mult['a'] = "0a7de493f5821b6c";
    mult['b'] = "0b5ea1f47c29d683";
    mult['c'] = "0cb759e2a61df348";
    mult['d'] = "0d941c852fb63ea7";
    mult['e'] = "0ef1d32c97684ab5";
    mult['f'] = "0fd2964b1ec3875a";
  }

  F16(int _x=0){
    /** Mas que simplemente modulear para aceptar valores negativos de _x **/
    x = _x;
  }


  static int size() {return 16; }

  bool operator!= (const F16 &o) const {
    return x != o.x;
  }
  bool operator== (const F16 &o) const {
    return x == o.x;
  }

  bool operator< (const F16 &o) const {
    if (isdigit(x) && isdigit(o.x))
      return x < o.x;
    if (!isdigit(x) && !isdigit(o.x))
      return x < o.x;
    if (isdigit(x) && !isdigit(o.x))
      return true;
    return false;
  }

  F16 operator+ (const F16 &o) const{
    return F16(add[(int)x][o.toInt()]);
  }
  F16 operator- (const F16 &o) const{
    return F16(add[(int)x][o.toInt()]);
  }

  F16 operator* (const F16 &o) const{
    return F16(mult[(int)x][o.toInt()]);
  }

  F16 inverso() const {
    return F16(_INVERSOS[toInt()]);
  }

  F16 operator/(const F16 &o) const {
    return (*this)*o.inverso();
  }

  F16 operator- () const {
    return F16(x);
  }



  ///Para imprimir con cout los elemntos de GF
  friend ostream& operator<< (ostream& os, const F16& a){
    os << a.x;
    return os;
  }

  const static F16& ZERO(){return _ZERO; }
  const static F16& ONE(){return _ONE; }
  const static vector<F16>& ELEMS(){return _ELEMS; }

};

F16 F16::_ZERO = '0';
F16 F16::_ONE = '1';
vector<F16> F16::_ELEMS = vector<F16> ({'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'});
vector<F16> F16::_INVERSOS = vector<F16> ({'0','1','9','e','d','b','7','6','f','2','c','5','a','4','3','8'});
string F16::add[256];
string F16::mult[256];

///Para imprimir con cout los vectores
ostream& operator<< (ostream& os, const vector< F16 > a){
  for (int j=0; j < (int)a.size(); j++){
    //if (j > 0) os << " ";
    os << a[j];
  }
  return os;
}

#endif // F16_H
