#ifndef GF_H
#define GF_H

#include<iostream>
using namespace std;

/**
  Este implementación de Galois Fields funciona si MOD es un numero primo
*/

template<int MOD>
class GF {
private:
  static vector<GF> _ELEMS;
  static GF _ZERO;
  static GF _ONE;

  int x;
public:

  GF(int _x=0){
    /** Mas que simplemente modulear para aceptar valores negativos de _x **/
    x = ((_x%MOD)+MOD)%MOD;
  }

  static int size() {
    return MOD;
  }

  bool operator!= (const GF &o) const {
    return x != o.x;
  }
  bool operator== (const GF &o) const {
    return x == o.x;
  }

  bool operator< (const GF &o) const {
    return x < o.x;
  }

  GF operator+ (const GF &o) const{
    return GF((x + o.x)%MOD);
  }
  GF operator- (const GF &o) const{
    return GF((MOD + x - o.x)%MOD);
  }

  GF operator* (const GF &o) const{
    return GF((x * o.x)%MOD);
  }

  GF operator- () const {
    return GF((MOD-x)%MOD);
  }

  ///Para imprimir con cout los elemntos de GF
  friend ostream& operator<< (ostream& os, const GF& a){
    os << a.x;
    return os;
  }

  const static GF& ZERO(){return _ZERO; }
  const static GF& ONE(){return _ONE; }
  const static GF& ELEMS(){
    if (_ELEMS.size()) return _ELEMS;
    _ELEMS = vector<GF<MOD>>(MOD);
    for (int i=0; i < MOD; i++)
      _ELEMS[i] = GF<MOD> (i);
    return _ELEMS;
  }
};

template<int MOD>
GF<MOD> GF<MOD>::_ZERO = 0;

template<int MOD>
GF<MOD> GF<MOD>::_ONE = 1;

template<int MOD>
vector<GF<MOD>> GF<MOD>::_ELEMS = vector<GF<MOD>>();


///Para imprimir con cout los vectores
template<int MOD>
ostream& operator<< (ostream& os, const vector< GF<MOD> > a){
  for (int j=0; j < (int)a.size(); j++){
    //if (j > 0) os << " ";
    os << a[j];
  }
  return os;
}


#endif // GF_H
