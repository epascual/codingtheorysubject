#ifndef LEncoderDecoderBYSTDARR_H_INCLUDED
#define LEncoderDecoderBYSTDARR_H_INCLUDED

#include "LEncoderDecoder.h"

/**
  Para decodificar por arreglo estandar
*/
template<class Field>
class LEncoderDecoderByStdArr : public LEncoderDecoder<Field> {

  vector<vector<vector<Field>>> standardArray;
  using LEncoderDecoder<Field>::G;
  using LEncoderDecoder<Field>::H;
  using LEncoderDecoder<Field>::standardGtoH;
  using LEncoderDecoder<Field>::standardHtoG;

  void generateCodes(int pos, int K, vector<vector<Field>> &codes, vector<Field> &info){
    if (pos == K){
      codes.push_back(this->encode(info));
      return;
    }
    for (int i=0; i < Field::size(); i++){
      info[pos] = Field (i);
      generateCodes(pos+1, K, codes, info);
    }
  }

  static void generateQN(int pos, int N, set<vector<Field>> QN[], vector<Field> &elem){
    if (pos == N){
      int weight = hammingWeight(elem);
      QN[weight].insert(elem);
      return;
    }
    for (int i=0; i < Field::size(); i++){
      elem[pos] = Field (i);
      generateQN(pos+1, N, QN, elem);
    }
  }

public:
  LEncoderDecoderByStdArr(const Matrix<Field> &_G, const Matrix<Field> &_H):
                      LEncoderDecoder<Field>(_G, _H){
    int n = G.getWidth(), k = G.getHeight();

    vector<vector<Field>> codes;
    vector<Field> info(k);
    generateCodes(0, k, codes, info);

    /** La primera fila son los c�digos */
    vector<
      set<vector<Field>>
    > QN(n+1);
    vector<Field> elem(n);
    generateQN(0, n, &QN[0], elem);

    for (int errorSize=0; errorSize < n+1; errorSize++){
      while (!(QN[errorSize].empty())){
        auto error = *(QN[errorSize].begin());
        vector<vector<Field>> row;
        for (auto code : codes){
          auto elem = code + error;
          row.push_back(elem);
          int weight = hammingWeight(elem);
          QN[weight].erase(QN[weight].find(elem));
        }
        standardArray.push_back(row);
      }
    }
  }

  static LEncoderDecoderByStdArr fromStandardG(const Matrix<Field > &G){
    Matrix<Field> H = standardGtoH(G);
    return LEncoderDecoderByStdArr(G, H);
  }
   static LEncoderDecoderByStdArr fromStandardH(const Matrix<Field > &H){
    Matrix<Field> G = standardHtoG(H);
    return LEncoderDecoderByStdArr(G, H);
  }

  virtual vector<Field> decode(const vector<Field> &code){
    for (int i=0; i < (int)standardArray.size(); i++){
      for (int j=0; j < (int)standardArray[i].size(); j++){
        if (standardArray[i][j] == code){
          return LEncoderDecoder<Field>::decode(code - standardArray[i][0]);
        }
      }
    }
    /* ESTO NO DEBERIA PASAR*/
    assert(false && "no encontro la palabra a descodificar");
    return LEncoderDecoder<Field>::decode(code);
  }

  void printStandardArray(){
    for (int i=0; i < (int)standardArray.size(); i++){
      for (int j=0; j < (int)standardArray[i].size(); j++){
        if (j) cout << " ";
        cout << standardArray[i][j];
      }
      cout << "\n";
    }
  }


/** Evaluation section */
private:
  double calculatePerr(double p){
    double ret = 1;
    int n = G.getWidth();
    vector<int> alpha(n+1, 0);
    for (int i=0; i < (int)standardArray.size(); i++){
      alpha[hammingWeight(standardArray[i][0])]++;
    }
    for (int j=0; j <= n; j++){
      ret -= alpha[j]*ipow(1.0-p,n-j)*ipow(p,j);
    }
    return ret;
  }

  inline double PC(int col, double p){
    int n = G.getWidth();
    double ret = 0;
    for (int i=0; i < (int)standardArray.size(); i++){
      int w = hammingWeight(standardArray[i][col]);
      ret += ipow(p,w)*ipow(1.0-p, n-w);
    }
    return ret;
  }

  double calculatePsymb(double p){
    double ret = 0;
    double k = G.getHeight();
    int qk = (int)standardArray[0].size();
    for (int i=0; i < qk; i++){
      double Fi = hammingWeight(standardArray[0][i], k);
      ret += Fi*PC(i,p);
    }
    return ret/k;
  }

  pair<double,double> calculatePundetectAndPret(double p){
    pair<double,double> ret = pair<double,double>(0,1.0);
    int n = G.getWidth();
    vector<int> A(n+1, 0);
    for (int i=0; i < (int)standardArray[0].size(); i++)
      A[hammingWeight(standardArray[0][i])]++;

    for (int i=1; i <= n; i++){
      ret.first += ((double)A[i])*ipow(1.0-p,n-i)*ipow(p,i);
    }

    ret.second -= pow(1.0-p,n);
    ret.second -= ret.first;
    return ret;
  }

  int calculateD(){
    int ret = G.getWidth(); ///N es la distancia m�xima
    for (int i=0; i < (int)standardArray[0].size(); i++){
      int w = hammingWeight(standardArray[0][i]);
      if (w > 0){
        ret = min(ret, w);
      }
    }
    return ret;
  }

  pair<int,int> calculateR(){
    int k = G.getHeight();
    int n = G.getWidth();
    int g = __gcd(n,k);
    k /= g;
    n /= g;
    return pair<int,int> (k,n);
  }

public:
  virtual void evaluate(double p){
    double Perr, Psymb, Pundetect, Pret;

    auto R = calculateR();
    int d = calculateD();
    Perr = calculatePerr(p);
    Psymb = calculatePsymb(p);
    auto PundetectAndPret = calculatePundetectAndPret(p);
    Pundetect = PundetectAndPret.first;
    Pret = PundetectAndPret.second;

    cout << "R         = " << R.first << "/" << R.second << "\n";
    cout << "d         = " << d << "\n";
    cout << "Perr      = " << Perr << "\n";
    cout << "Psymb     = " << Psymb << "\n";
    cout << "Pundetect = " << Pundetect << "\n";
    cout << "Pret      = " << Pret << "\n";
  }

};


#endif // LEncoderDecoderBYSTDARR_H_INCLUDED
