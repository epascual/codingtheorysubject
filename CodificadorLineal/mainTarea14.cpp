#include <bits/stdc++.h>
#include "Matrix.h"
#include "LEncoderDecoder.h"
#include "LEncoderDecoderByStdArr.h"
#include "LEncoderDecoderBySyndrome.h"
#include "F16EncoderDecoder.h"
#include "F16.h"
using namespace std;


vector<F16> encode(LEncoderDecoder<F16> &led, string cad){
  vector<F16> ex1 = vector<F16>(cad.size());
  for (int i=0; i < (int)cad.size(); i++){
    ex1[i] = F16(cad[i]);
  }
  cout << cad << " -> ";
  return led.encode(ex1);
}

vector<F16> decode(LEncoderDecoder<F16> &led, string cad){
  vector<F16> ex1 = vector<F16>(cad.size());
  for (int i=0; i < (int)cad.size(); i++){
    ex1[i] = F16(cad[i]);
  }
  cout << cad << " -> ";
  return led.decode(ex1);
}

int main()
{
  F16::init();

  cout << "Tarea 14\n";
  cout << "Eduardo Pascual Aseff\n";

  Matrix<F16> H ( 2, 17, {
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','0' },
    { '1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','0','1' }
  });

  F16EncoderDecoder tarea14 =
    F16EncoderDecoder::fromStandardH(H);

//  cout << "Examples encode\n";
//  cout << encode(tarea14,"0aa19efc3ffd21c") << "\n";
//  cout << encode(tarea14,"524f80f19b87723") << "\n";
//  cout << encode(tarea14,"54f938ef8f9022c") << "\n\n";

  cout << "Codificaciones\n";
  cout << encode(tarea14,"3b6936970aa6bb2") << "\n";
  cout << encode(tarea14,"3ccca6e03f90bcb") << "\n";
  cout << encode(tarea14,"43e25bebcd9989c") << "\n";
  cout << encode(tarea14,"524f80f19b87723") << "\n";
  cout << encode(tarea14,"35a769c963d8edc") << "\n";
  cout << encode(tarea14,"d87d0bf8b531067") << "\n";
  cout << encode(tarea14,"592492573935517") << "\n";
  cout << encode(tarea14,"fd979d0aab59390") << "\n";
  cout << encode(tarea14,"6022009d1403d9b") << "\n";
  cout << encode(tarea14,"0b53623ac2f2671") << "\n\n";

//  cout << "Examples decode\n";
//  cout << decode(tarea14,"5d748b824b29873e9") << "\n";
//  cout << decode(tarea14,"10370871c9cd8fd51") << "\n";
//  cout << decode(tarea14,"3880d1a5a7ebda0ab") << "\n\n";

  cout << "Decodificaciones\n";
  cout << decode(tarea14,"b3ea094f0038e1c78") << "\n";
  cout << decode(tarea14,"c497cfebcc846550f") << "\n";
  cout << decode(tarea14,"5d748b824b29873e9") << "\n";
  cout << decode(tarea14,"10370871c9cd8fd51") << "\n";
  cout << decode(tarea14,"0c2a9da563924e177") << "\n";
  cout << decode(tarea14,"213a673a150154f38") << "\n";
  cout << decode(tarea14,"b6a6ef297e4353327") << "\n";
  cout << decode(tarea14,"e8175ed67dece71c6") << "\n";
  cout << decode(tarea14,"3880d1a5a7ebda0ab") << "\n";
  cout << decode(tarea14,"f9be794b1364fb3d9") << "\n";

  return 0;
}
