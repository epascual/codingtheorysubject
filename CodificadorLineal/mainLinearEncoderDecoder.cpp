#include <bits/stdc++.h>
#include "Matrix.h"
#include "LEncoderDecoder.h"
#include "LEncoderDecoderByStdArr.h"
#include "LEncoderDecoderBySyndrome.h"
#include "GF.h"
using namespace std;

/* Shortcut para referirse a GF2 */
typedef GF<2> GF2;
typedef GF<3> GF3;

/**Porque en los ejemplos trabajaremos sobre todo con el campo binario*/
typedef LEncoderDecoder<GF2> BinEncoderDecoder;

int main()
{
  cout << fixed << setprecision(8);
  Matrix<GF2> H (3, 6,  {
    {0,1,1,1,0,0},
    {1,0,1,0,1,0},
    {1,1,0,0,0,1}
  }), G (3, 6,{
    {1,0,0,0,1,1},
    {0,1,0,1,0,1},
    {0,0,1,1,1,0}
  });

  cout << "Testing if encode works: \n";
  BinEncoderDecoder led(G, H);
  vector<GF2> v = {0,1,1};
  cout << led.encode(v) << "\n";
  //cout << led.decode(led.encode(v)) << "\n";

  cout << "\nTesting fromStandardG\n";
  BinEncoderDecoder led2 = BinEncoderDecoder::fromStandardG(G);
  cout << led2 << "\n";

  cout << "Testing fromStandardH\n";
  BinEncoderDecoder led3 = BinEncoderDecoder::fromStandardH(H);
  cout << led3 << "\n";

  LEncoderDecoderByStdArr<GF2> led4(G,H);
  led4.printStandardArray();

  Matrix<GF2> G_example9 (2, 4,  {
    {1,0,1,1},
    {0,1,0,1}
  }), G_example10 (2, 5,{
    {1,0,1,1,1},
    {0,1,1,0,1}
  });
//
  LEncoderDecoderByStdArr<GF2> ledex9 =
        LEncoderDecoderByStdArr<GF2>::fromStandardG(G_example9);
  vector<GF2> v9 = {1,0,1,0};
  cout << ledex9.decode(v9) << "\n";
  ledex9.printStandardArray();

  cout << "\n";
  LEncoderDecoderByStdArr<GF2> ledex10 =
        LEncoderDecoderByStdArr<GF2>::fromStandardG(G_example10);
  vector<GF2> v10 = {1,0,0,1,0};
  cout << ledex10.decode(v10) << "\n";
  ledex10.printStandardArray();



  LEncoderDecoderBySyndrome<GF2> ledex11 =
        LEncoderDecoderBySyndrome<GF2>::fromStandardG(G_example10);
  cout << ledex11.decode(v10) << "\n";
  ledex11.printSyndromes();

  cout << "Testing evaluation!\n";
  LEncoderDecoderBySyndrome<GF2> ledEval =
        LEncoderDecoderBySyndrome<GF2>::fromStandardG(G_example9);
  ledEval.evaluate(0.01);

  LEncoderDecoderBySyndrome<GF2> ledEval2 =
        LEncoderDecoderBySyndrome<GF2>::fromStandardG(G);
  ledEval2.evaluate(0.01);


  cout << "Testing evaluation!\n";
  LEncoderDecoderByStdArr<GF2> ledEval3 =
        LEncoderDecoderByStdArr<GF2>::fromStandardG(G_example9);
  ledEval3.evaluate(0.01);


  LEncoderDecoderByStdArr<GF2> ledEval4 =
        LEncoderDecoderByStdArr<GF2>::fromStandardG(G);
  ledEval4.evaluate(0.01);

  return 0;
}
