#include <bits/stdc++.h>
#include "Matrix.h"
#include "GF.h"
using namespace std;

/* Shortcut para referirse a GF2 */
typedef GF<2> GF2;

/* Verifica si pertenece al kernel de H */
bool isCodeword(const Matrix<GF2> &H, const vector<GF2> &X){
  vector<GF2> mult = H*X;
  for (GF2 elem : mult){
    if (elem != GF2::ZERO()) return false;
  }
  return true;
}

/* Auxiliar para generar todos los vectores del campo binario GF2 */
void generaFN_aux(int pos, int N, vector<GF2> &tmp, vector<vector<GF2> > &FN){
  if (pos == N){
    FN.push_back(tmp);
    return;
  }
  tmp[pos].x = 0;
  generaFN_aux(pos+1, N, tmp, FN);
  tmp[pos].x = 1;
  generaFN_aux(pos+1, N, tmp, FN);
}


/* Para generar todos los vectores del campo binario FN */
void generaFN(int N, vector<vector<GF2> > &FN){
  vector<GF2> tmp(N);
  generaFN_aux(0, N, tmp, FN);
}

int main()
{
  vector< vector<GF2> > F4;
  generaFN(4, F4);

  Matrix<GF2> H1(2,4,{{1,0,1,0},{1,1,0,1}});

  cout << "Codes de H1\n";
  for (int i=0; i < (int)F4.size(); i++){
    if (isCodeword(H1, F4[i])){
      cout << "\t" << F4[i] << "\n";
    }
  }

  Matrix<GF2> H2(2,4,{{0,1,1,1},{1,1,0,1}});

  cout << "Codes de H2\n";
  for (int i=0; i < (int)F4.size(); i++){
    if (isCodeword(H2, F4[i])){
      cout << "\t" << F4[i] << "\n";
    }
  }

  vector< vector<GF2> > F7;
  generaFN(7,F7);

  Matrix<GF2> H3(3,7,{
    {0,1,1,1,1,0,0},
    {1,0,1,1,0,1,0},
    {1,1,0,1,0,0,1}});

  cout << "Codes de H3\n";

  for (int i=0; i < (int)F7.size(); i++){
    if (isCodeword(H3, F7[i])){
      cout << "\t" << F7[i] << "\n";
    }
  }

  return 0;
}
