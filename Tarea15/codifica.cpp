#include <stdio.h>

/*  Palabras de codigo */
unsigned char PC[16] = {0x00,0x1e,0x2d,0x33,0x4b,0x55,0x66,0x78,
		        0x87,0x99,0xaa,0xb4,0xcc,0xd2,0xe1,0xff};

int main(int argc, char *argv[])
{
	FILE *fp, *fout;
	int  c;

	if((fp = fopen(argv[1],"rb")) == NULL) {
	    printf("Error en el archivo %s\n",argv[1]);
	    return 0;
	}
	if((fout = fopen(argv[2],"wb")) == NULL) {
	    printf("Error en el archivo %s\n",argv[1]);
	    return 0;
	}
	while((c=fgetc(fp))!=EOF) {
	    fputc(PC[c&15], fout);
	    fputc(PC[(c>>4)&15], fout);
	}
	fclose(fp);
	fclose(fout);
}
