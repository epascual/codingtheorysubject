#/bin/bash

if [[ $# -lt 1 ]]
then
	echo "Usage: $0 [file_to_code]"
	exit
fi


echo "Coding $1"

./codifica $1 $1_coded

echo "In canal"

./canal 100 $1_coded $1_noise

echo "Evaluating"
echo ""

#./decodifica $1 $1_noise
./decodifica $1_noise $1
