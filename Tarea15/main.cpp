#include <bits/stdc++.h>

using namespace std;

typedef unsigned long long ll;
typedef unsigned char uchar;

#define N 8

/*  Palabras de codigo */
unsigned char PC[16] = {0x00,0x1e,0x2d,0x33,0x4b,0x55,0x66,0x78,
                        0x87,0x99,0xaa,0xb4,0xcc,0xd2,0xe1,0xff};

/* Error estimado por cada s�ndrome posible */
unsigned char EI[16] = {0x00,0x03,0x05,0x06,0x09,0x0a,0x0c,0x11,
                        0x01,0x02,0x04,0x80,0x08,0x40,0x20,0x10};

/**
 * Peso de un vector v:
 *  Cantidad de bits activos
 */
int weight(int v){
  int ret = 0;
  while (v){
    ret += (v & 1);
    v >>= 1;
  }
  assert(ret <= N);
  return ret;
}

/**
 * Imprime en pantalla los 8 bits de un vector binario
 * Solo para debug
 */
void printVector(int x){
  char cad[10];
  cad[8] = 0;
  for (int i=0, j=7; i < 8; i++, j--){
    cad[j] = '0';
    if (x & (1 << i)) cad[j]++;
  }
  printf("%s\n",cad);
}

/* Matriz H (chequeo de paridad) del C�digo utilizado */
int H[4] = {
  0b11111111,
  0b01111000,
  0b10110100,
  0b11010010
};

/* Multiplicacion de H por un vector X */
int Hmult(int X){
  return ((weight(H[0]&X)&1)<<3) ^
         ((weight(H[1]&X)&1)<<2) ^
         ((weight(H[2]&X)&1)<<1) ^
         ((weight(H[3]&X)&1)<<0);
}

/********************************************
 * U - palabra de informaci�n real          *
 * y - vector que recibe el decodificador   *
 ********************************************/
ll WtXy[N+1], WtUw[N+1], WtXX_[N+1], WtUU_[N+1];
void do_it(int U, int y){
  int X = PC[U];
  /** X>>4 es lo mismo que U */

  WtXy[weight(X ^ y)]++;
  WtUw[weight((X>>4)^(y>>4))]++;

  /**
   * Hmult(y) : es H*y, el sindrome para y
   * EI[Hmult(y)] : es el error para el s�ndrome de y
   * X_ = EI[Hmult(y)]^y: es la palabra de c�digo estimada a partir de y
   */
  int X_ = EI[Hmult(y)]^y;
  WtXX_[weight(X ^ X_)]++;
  WtUU_[weight((X>>4)^(X_>>4))]++;
}

int main(int args, char *argv[]){

  FILE *fp, *fin;
	int  c, y;

	if((fp = fopen(argv[1],"rb")) == NULL) {
	    printf("Error en el archivo %s\n",argv[1]);
	    return 0;
	}
	if((fin = fopen(argv[2],"rb")) == NULL) {
	    printf("Error en el archivo %s\n",argv[2]);
	    return 0;
	}

	while((c=fgetc(fp))!=EOF) {
    y = fgetc(fin);
    do_it(c&15, y);

    y = fgetc(fin);
    do_it((c>>4)&15, y);
	}

	fclose(fp);
	fclose(fin);

	printf(  "                  | %10s | %10s | %10s | %10s\n",
                              "Wt(X-y)", "Wt(U-w)", "Wt(X-X^)", "Wt(U-U^)");
	for (int i=0; i <= N; i++){
    printf("Errores de peso %d | %10I64d | %10I64d | %10I64d | %10I64d \n", i,
                                 WtXy[i], WtUw[i], WtXX_[i], WtUU_[i]);
	}

	/*
	 * Calculo de error producido por el canal por bit
	 */
  long long bitsReceived = 0;
	long long bitsAltered = 0;
	for (int i=0; i <= N; i++){
    bitsReceived += WtXy[i]*8;
    bitsAltered += WtXy[i]*i;
	}
	double P = (double)bitsAltered/(double)bitsReceived;
	printf("P(canal) = %I64d/%I64d = %.8f\n",
        bitsAltered, bitsReceived, P);

  /*
   * Probabilidad de c�digos que no se corrigen
   */
  long long XReceived = 0;
	long long XError = 0;
	for (int i=0; i <= N; i++){
    XReceived += WtXX_[i];
    if (i > 0)
      XError += WtXX_[i];
	}
	P = (double)XError/(double)XReceived;
	printf("P_err = %I64d/%I64d = %.8f\n",
        XError, XReceived, P);

  /*
   * Probabilidad de error por s�mbolo PSym
   */
  long long UBitsReceived = 0;
	long long UBitsError = 0;
	for (int i=0; i <= N; i++){
    UBitsReceived += 4*WtUU_[i];
    if (i > 0)
      UBitsError += i*WtUU_[i];
	}
	P = (double)UBitsError/(double)UBitsReceived;
	printf("P_sym = %I64d/%I64d = %.8f\n",
        UBitsError, UBitsReceived, P);

  return 0;
}
